var inputExpression = prompt("Expression:");
var rpn = convertToReversePolishNotation(inputExpression);
var result = calculateExpression(rpn);

document.writeln(rpn + "</br>");
document.writeln(result);

function convertToReversePolishNotation(expression) {
  var outputQueue = [];
  var operatorStack = [];

  for (let index = 0; index < expression.length; index++) {
    const token = expression[index];
    if (isNumber(token)) {
      outputQueue.push(token);
    } else if (token == "(") {
      operatorStack.push(token);
    } else if (isOperator(token)) {
      while (getPriority(token) <= getPriority(operatorStack[operatorStack.length - 1])) {
        outputQueue.push(operatorStack.pop());
      }

      operatorStack.push(token);
    } else if (token == ")") {
      while (operatorStack[operatorStack.length - 1] != "(") {
        if (operatorStack.length == 0) {
          throw "Error";
        }
        outputQueue.push(operatorStack.pop());
      }

      operatorStack.pop();
    }
  }

  while (operatorStack.length != 0) {
    if (!operatorStack[operatorStack.length - 1].match(/([()])/)) {
      outputQueue.push(operatorStack.pop());
    } else {
      throw "Error";
    }
  }

  return outputQueue.join(" ");
}

function calculateExpression(expression) {
  var evalStack = [];
  for (let index = 0; index < expression.length; index++) {
    const token = expression[index];

    if (isNumber(token)) {
      evalStack.push(token);
    } else if (isOperator(token)) {
      var operand1 = evalStack.pop();
      var operand2 = evalStack.pop();

      var result = calculateOperation(parseInt(operand1), parseInt(operand2), token);
      evalStack.push(result);
    }
  }
  return evalStack.pop();
}

function calculateOperation(operand1, operand2, operator) {
  switch (operator) {
    case "+":
      return operand1 + operand2;
    case "-":
      return operand1 - operand2;
    case "*":
      return operand1 * operand2;
    case "/":
      return operand1 / operand2;
    default:
      return;
  }
}

function isOperator(token) {
  return ["+", "-", "*", "/"].indexOf(token) > -1;
}

function isNumber(token) {
  return token.match(/([0-9]+)/);
}

function getPriority(token) {
  switch (token) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
      return 2;
    default:
      return -1;
  }
}
