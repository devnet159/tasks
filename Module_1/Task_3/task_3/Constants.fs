﻿module Constants
[<Literal>] 
let PLUS_OPERATOR = "+"
[<Literal>] 
let MINUS_OPERATOR = "-"
[<Literal>] 
let MULTIPLICATION_OPERATOR = "*"
[<Literal>] 
let DIVISION_OPERATOR = "/"
