﻿open System
open System.Text.RegularExpressions

// конвертируем строку в список
let ExpressionToList (expr: string) =
    expr |> (new Regex("\s+|\s*([-+*/])\s*")).Split
         |> Array.toList
         |> List.filter(fun s -> s.Length > 0)

// расчет оператора
// https://docs.microsoft.com/ru-ru/dotnet/fsharp/language-reference/pattern-matching
let CalculateOperation(token: string) (stack: int list) = 
    match (token, stack) with        
    | (Constants.PLUS_OPERATOR, a :: b :: cs) -> (b + a) :: cs 
    | (Constants.MINUS_OPERATOR, a :: b :: cs) -> (b - a) :: cs
    | (Constants.MULTIPLICATION_OPERATOR, a :: b :: cs) -> (b * a) :: cs
    | (Constants.DIVISION_OPERATOR, a :: b :: cs) -> (b / a) :: cs
    | (n, cs) -> int n :: cs

// расчет выражения 
// https://docs.microsoft.com/ru-ru/dotnet/fsharp/language-reference/lists#recursion-with-lists
let CalculateRpn (expression: string list) =
    let rec CalculateRecur (expression: string list) (stack: int list) =
        match expression with
            | [] -> stack
            | token :: exp -> CalculateRecur exp (CalculateOperation token stack)
    CalculateRecur expression []

[<EntryPoint>]
let main argv =         
    Console.ReadLine() |> ExpressionToList |> CalculateRpn |> printfn "%O"        
    0
