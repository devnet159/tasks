﻿using System;

namespace task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            ICalculator calculator = new Calculator();

            Console.WriteLine("Expression:");
            string expression = Console.ReadLine();
            string rpnExpression = calculator.ConvertToReversePolishNotation(expression);
            int result = calculator.CalculateRpnExpression(rpnExpression);

            Console.WriteLine(rpnExpression);
            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
