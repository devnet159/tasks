﻿namespace task_2
{
    public static class Constants
    {
        public static class Operators
        {
            public const string PLUS = "+";
            public const string MINUS = "-";
            public const string MULTIPLICATION = "*";
            public const string DIVISION = "/";
        }

        public static class Tokens
        {
            public const string OPENING_BRACKET = "(";
            public const string CLOSING_BRACKET = ")";
        }
    }
}
