﻿using System;
using System.Collections.Generic;

namespace task_2
{
    public class Calculator : BaseCalculator, ICalculator
    {
        public int CalculateRpnExpression(string rpnExpression)
        {
            Stack<int> evalStack = new Stack<int>();
            for (var index = 0; index < rpnExpression.Length; index++)
            {
                string token = rpnExpression.Substring(index, 1);

                if (IsNumber(token))
                {
                    evalStack.Push(int.Parse(token));
                    continue;
                }

                if (IsOperator(token))
                {
                    int leftOperand = evalStack.Pop();
                    int rightOperand = evalStack.Pop();
                    int result = CalculateOperation(leftOperand, rightOperand, token);
                    evalStack.Push(result);
                }
            }

            return evalStack.Pop();
        }

        public string ConvertToReversePolishNotation(string expression)
        {
            var outputQueue = new Queue<string>();
            var operatorStack = new Stack<string>();

            for (var index = 0; index < expression.Length; index++)
            {
                string token = expression.Substring(index, 1);
                if (IsNumber(token))
                {
                    outputQueue.Enqueue(token);
                    continue;
                }

                if (token == Constants.Tokens.OPENING_BRACKET)
                {
                    operatorStack.Push(token);
                    continue;
                }

                if (IsOperator(token))
                {
                    while (operatorStack.Count > 0 && GetPriority(token) <= GetPriority(operatorStack.Peek()))
                    {
                        outputQueue.Enqueue(operatorStack.Pop());
                    }

                    operatorStack.Push(token);
                    continue;
                }

                if (token == Constants.Tokens.CLOSING_BRACKET)
                {
                    while (operatorStack.Peek() != Constants.Tokens.OPENING_BRACKET)
                    {
                        if (operatorStack.Count == 0)
                        {
                            throw new Exception("Error");
                        }

                        outputQueue.Enqueue(operatorStack.Pop());
                    }

                    operatorStack.Pop();
                }
            }

            while (operatorStack.Count != 0)
            {
                if (operatorStack.Peek() != Constants.Tokens.OPENING_BRACKET && operatorStack.Peek() != Constants.Tokens.CLOSING_BRACKET)
                {
                    outputQueue.Enqueue(operatorStack.Pop());
                }
                else
                {
                    throw new Exception("Error");
                }
            }

            return string.Join(" ", outputQueue);
        }
    }
}
