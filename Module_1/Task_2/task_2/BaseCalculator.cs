﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace task_2
{
    public abstract class BaseCalculator
    {
        protected virtual string[] GetOperators()
        {
            return new string[] {
                Constants.Operators.PLUS,
                Constants.Operators.MINUS,
                Constants.Operators.MULTIPLICATION,
                Constants.Operators.DIVISION
            };
        }

        protected virtual bool IsOperator(string token)
        {
            return GetOperators().Contains(token);
        }

        protected virtual bool IsNumber(string token)
        {
            Regex regex = new Regex(@"^[0-9]+$");
            return regex.IsMatch(token);
        }

        protected virtual int GetPriority(string token)
        {
            switch(token)
            {
                case Constants.Operators.PLUS:
                case Constants.Operators.MINUS:
                    return 1;
                case Constants.Operators.MULTIPLICATION:
                case Constants.Operators.DIVISION:
                    return 2;
                default:
                    return -1;
            };
        }

        protected virtual int CalculateOperation(int leftOperand, int rightOperand, string operationOperator)
        {
            return operationOperator switch
            {
                Constants.Operators.PLUS => leftOperand + rightOperand,
                Constants.Operators.MINUS => leftOperand - rightOperand,
                Constants.Operators.MULTIPLICATION => leftOperand * rightOperand,
                Constants.Operators.DIVISION => leftOperand / rightOperand,
                _ => 0,
            };
        }
    }
}
