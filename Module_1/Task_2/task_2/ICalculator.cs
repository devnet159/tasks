﻿namespace task_2
{
    public interface ICalculator
    {
        string ConvertToReversePolishNotation(string expression);
        int CalculateRpnExpression(string rpnExpression);
    }
}
