SET IDENTITY_INSERT [User] ON;
INSERT INTO [User] (Id, LastName) VALUES(1, N'Иванов');
INSERT INTO [User] (Id, LastName) VALUES(2, N'Петров');
INSERT INTO [User] (Id, LastName) VALUES(3, N'Сидоров');
INSERT INTO [User] (Id, LastName) VALUES(4, N'Козлов');
INSERT INTO [User] (Id, LastName) VALUES(5, N'Орлов');
INSERT INTO [User] (Id, LastName) VALUES(6, N'Иванов');
SET IDENTITY_INSERT [dbo].[User] OFF;

SET IDENTITY_INSERT [PhoneBook] ON;
INSERT INTO PhoneBook (Id, UserId, PhoneNumber) VALUES(1, 1, '111-111');
INSERT INTO PhoneBook (Id, UserId, PhoneNumber) VALUES(2, 2, '222-222');
INSERT INTO PhoneBook (Id, UserId, PhoneNumber) VALUES(3, 3, '333-333');
INSERT INTO PhoneBook (Id, UserId, PhoneNumber) VALUES(4, 4, '444-444');
INSERT INTO PhoneBook (Id, UserId, PhoneNumber) VALUES(5, 5, '555-555');
SET IDENTITY_INSERT [PhoneBook] OFF;

SET IDENTITY_INSERT Address ON;
INSERT INTO Address (Id, City, Street, HouseNumber, ApartmentNumber, UserId) VALUES(1, N'Москва', N'ул. Ленина', '1', '1', 1);
INSERT INTO Address (Id, City, Street, HouseNumber, ApartmentNumber, UserId) VALUES(2, N'Санкт-Петербург', N'ул. Космонавтов', '2', '2', 2);
INSERT INTO Address (Id, City, Street, HouseNumber, ApartmentNumber, UserId) VALUES(3, N'Екатеринбург', N'ул. 8 марта', '3', '3', 3);
INSERT INTO Address (Id, City, Street, HouseNumber, ApartmentNumber, UserId) VALUES(4, N'Екатеринбург', N'ул. 8 марта', '4', '4', 4);
INSERT INTO Address (Id, City, Street, HouseNumber, ApartmentNumber, UserId) VALUES(5, N'Пермь', N'ул. Мира', '5', '5', 5);
INSERT INTO Address (Id, City, Street, HouseNumber, ApartmentNumber, UserId) VALUES(6, N'Пермь', N'ул. Мира', '6', '6', 6);
SET IDENTITY_INSERT Address OFF;

SET IDENTITY_INSERT Car ON;
INSERT INTO Car (Id, UserId, Model, Color, Price) VALUES(1, 1, N'BMW 5-series', N'Black', 3000000);
INSERT INTO Car (Id, UserId, Model, Color, Price) VALUES(2, 1, N'BMW X5 M', N'Red', 9000000);
INSERT INTO Car (Id, UserId, Model, Color, Price) VALUES(3, 1, N'BMW Z4 Roadster', N'Blue', 3500000);
INSERT INTO Car (Id, UserId, Model, Color, Price) VALUES(4, 2, N'Hyundai Solaris', N'White', 800000);
INSERT INTO Car (Id, UserId, Model, Color, Price) VALUES(5, 2, N'Газель', N'White', 1500000);
INSERT INTO Car (Id, UserId, Model, Color, Price) VALUES(6, 3, N'Lada Granta', N'Black', 500000);
INSERT INTO Car (Id, UserId, Model, Color, Price) VALUES(7, 4, N'Lada Granta', N'Red', 500000);
SET IDENTITY_INSERT Car OFF;

SET IDENTITY_INSERT BankDepositor ON;
INSERT INTO BankDepositor (Id, UserId, BankName, BankAccount, DepositAmount) VALUES(1, 1, N'Сбербанк', '1111-1111', 10000000);
INSERT INTO BankDepositor (Id, UserId, BankName, BankAccount, DepositAmount) VALUES(2, 2, N'Сбербанк', '2222-2222', 2000000);
INSERT INTO BankDepositor (Id, UserId, BankName, BankAccount, DepositAmount) VALUES(3, 3, N'Сбербанк', '3333-3333', 3000000);
INSERT INTO BankDepositor (Id, UserId, BankName, BankAccount, DepositAmount) VALUES(4, 4, N'Альфа-банк', '4444-4444', 4000000);
INSERT INTO BankDepositor (Id, UserId, BankName, BankAccount, DepositAmount) VALUES(5, 5, N'ВТБ', '5555-55555', 5000000);
SET IDENTITY_INSERT BankDepositor OFF;
