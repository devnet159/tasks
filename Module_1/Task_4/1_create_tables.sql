CREATE TABLE [User] (
	Id bigint IDENTITY(0,1) NOT NULL,
	LastName varchar(1000) COLLATE Cyrillic_General_CI_AS NOT NULL,
	CONSTRAINT User_PK PRIMARY KEY (Id)
);

CREATE TABLE PhoneBook (
	Id bigint IDENTITY(0,1) NOT NULL,
	UserId bigint NOT NULL,
	PhoneNumber varchar(50) COLLATE Cyrillic_General_CI_AS NOT NULL,
	CONSTRAINT PhoneBook_PK PRIMARY KEY (Id),
	CONSTRAINT FK_PhoneBook_UserId_User_Id FOREIGN KEY (UserId) REFERENCES [User](Id)
);

CREATE TABLE Address (
	Id bigint IDENTITY(0,1) NOT NULL,
	City varchar(100) COLLATE Cyrillic_General_CI_AS NULL,
	Street varchar(100) COLLATE Cyrillic_General_CI_AS NULL,
	HouseNumber varchar(100) COLLATE Cyrillic_General_CI_AS NULL,
	ApartmentNumber varchar(100) COLLATE Cyrillic_General_CI_AS NULL,
	UserId bigint NULL,
	CONSTRAINT Address_PK PRIMARY KEY (Id),
	CONSTRAINT FK_Address_UserId_User_Id FOREIGN KEY (UserId) REFERENCES [User](Id)
);

CREATE TABLE Car (
	Id bigint IDENTITY(0,1) NOT NULL,
	UserId bigint NOT NULL,
	Model varchar(100) COLLATE Cyrillic_General_CI_AS NOT NULL,
	Color varchar(100) COLLATE Cyrillic_General_CI_AS NOT NULL,
	Price decimal(38,0) NOT NULL,
	CONSTRAINT FK_Car_UserId_User_Id FOREIGN KEY (UserId) REFERENCES [User](Id)
);

CREATE TABLE BankDepositor (
	Id bigint IDENTITY(0,1) NOT NULL,
	UserId bigint NULL,
	BankName varchar(100) COLLATE Cyrillic_General_CI_AS NULL,
	BankAccount varchar(100) COLLATE Cyrillic_General_CI_AS NULL,
	DepositAmount decimal(38,2) NULL,
	CONSTRAINT FK_BankDepositor_UserId_User_Id FOREIGN KEY (UserId) REFERENCES [User](Id)
);
