-- а) По № телефона найти: Фамилию, Марку автомобиля, Стоимость автомобиля (может быть несколько)
 SELECT
	u.LastName,
	c.Model,
	c.Price
FROM
	[User] u
INNER JOIN PhoneBook pb ON
	pb.UserId = u.Id
INNER JOIN Car c on
	c.UserId = u.Id
WHERE
	pb.PhoneNumber = '111-111';
	
-- 	в) Используя сформированное в пункте а) правило, по № телефона найти: только Марку автомобиля (автомобилей может быть несколько)	 
SELECT
	c.Model
FROM
	[User] u
INNER JOIN PhoneBook pb ON
	pb.UserId = u.Id
INNER JOIN Car c on
	c.UserId = u.Id
WHERE
	pb.PhoneNumber = '111-111';
	
-- с) Используя простой, не составной вопрос: по Фамилии (уникальна в городе, но в разных городах есть однофамильцы) 
-- и Городу проживания найти:  Улицу проживания, Банки, в которых есть вклады и №телефона	
 SELECT
	a.Street,
	bd.BankName,
	pb.PhoneNumber
FROM
	[User] u
INNER JOIN PhoneBook pb ON
	pb.UserId = u.Id
INNER JOIN Address a on
	a.UserId = u.Id
INNER JOIN BankDepositor bd ON
	bd.UserId = u.Id
WHERE
	u.LastName = N'Иванов'
	and a.City = N'Москва';